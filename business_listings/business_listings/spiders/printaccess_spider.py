import scrapy
import json
from business_listings.items import BusinessListingsItem
from scrapy.loader import ItemLoader
import requests
import re


class PlayersSpider(scrapy.Spider):
    name = "printaccess"

    def start_requests(self):
        urls = ["https://www.printaccess.com/api/geobounds/?limit=-1"
                "&southWestLat=24.24982730548774&southWestLng=-142.22908423620248"
                "&northEastLat=48.02142426526739&northEastLng=-61.36970923620248"
                "&lat=37.057682769228755&lon=-101.79939673620248&aff=36"]
        for url in urls:
            request = scrapy.Request(url=url, callback=self.listing_parser)
            yield request


    def listing_parser(self, response):
        json_response = json.loads(response.body_as_unicode())
        for listing in json_response:
            l = ItemLoader(item=BusinessListingsItem())
            l.add_value("name", listing["name"])
            l.add_value("email", listing["email1"])
            l.add_value("phone", listing["phone1"])
            l.add_value("street", listing["address"])
            l.add_value("state", listing["state"])
            l.add_value("city", listing["city"])
            l.add_value("zip", listing["zipcode"])
            # l.add_value("ratingCount", listing["ratingCount"])
            # l.add_value("averageRating", listing["averageRating"])
            l.add_value("latitude", listing["latitude"])
            l.add_value("longitude", listing["longitude"])
            # l.add_value("categories", listing["categories"])
            # l.add_value("openHours", listing["openHours"])
            l.add_value("websiteURL", listing["url"])
            yield l.load_item()