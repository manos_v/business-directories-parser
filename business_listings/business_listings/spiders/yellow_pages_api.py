import scrapy
import json
from business_listings.items import BusinessListingsItem
from scrapy.loader import ItemLoader
import requests
import re

def get_email(website):
    r = requests.get(website, timeout = 4).text
    emails = re.findall(r'[\w\.-]+@[\w\.-]+', r)
    try:
        return emails[0].lower()
    except:
        return ""


class PlayersSpider(scrapy.Spider):
    name = "yp"

    def start_requests(self):
        kw = "Printing Services".replace(" ", "+")



        starting_url = 'http://pubapi.yp.com/search-api/search/devapi/search?searchloc={STATE}&term={KW}&format=json&key=dssm3q7x4p&pagenum={PN}'\
                       .replace("{KW}", kw)
        states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
                  "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
                  "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
                  "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
                  "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]
        for state in states:
            for i in range(1, 600):
                request = scrapy.Request(url=starting_url.replace("{STATE}", state).replace("{PN}", str(i)),
                                         callback=self.listing_parser)
                yield request



    def listing_parser(self, response):
        json_response = json.loads(response.body_as_unicode())
        for listing in json_response["searchResult"]["searchListings"]["searchListing"]:
            try:
                website = re.findall("dest=(.*)", listing["websiteURL"])[0].replace("%2F", "/").replace("%3A", ":")
            except:
                website = ""

            if listing["email"] == "" and website != "":
                email = get_email(listing["websiteURL"])
            else:
                email = listing["email"]
            l = ItemLoader(item=BusinessListingsItem())
            l.add_value("name", listing["businessName"])
            l.add_value("email", email)
            l.add_value("phone", listing["phone"])
            l.add_value("street", listing["street"])
            l.add_value("state", listing["state"])
            l.add_value("city", listing["city"])
            l.add_value("zip", listing["zip"])
            l.add_value("ratingCount", listing["ratingCount"])
            l.add_value("averageRating", listing["averageRating"])
            l.add_value("latitude", listing["latitude"])
            l.add_value("longitude", listing["longitude"])
            l.add_value("categories", listing["categories"])
            l.add_value("openHours", listing["openHours"])
            l.add_value("websiteURL", website)
            yield l.load_item()



